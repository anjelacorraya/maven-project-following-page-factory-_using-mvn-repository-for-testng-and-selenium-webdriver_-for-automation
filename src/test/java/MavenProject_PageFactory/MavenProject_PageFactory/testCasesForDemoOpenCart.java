package MavenProject_PageFactory.MavenProject_PageFactory;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.com.demoOpenCart.HomePage;
import pages.com.demoOpenCart.Registration;

public class testCasesForDemoOpenCart {
	
	WebDriver driver = new ChromeDriver();
		
	//Create Objects of All Pages
	
	HomePage homepage = new HomePage(driver);
	Registration registration = new Registration(driver);
	
	@BeforeTest
	public void beforeTestMethod() {
	driver.get("https://demo.opencart.com/");	
		
	}
	

//-------------Test Cases for Registration Process-----------------------------------------------------------------------------------------//
	@Test
	public void Verify_Negative_Registration_With_Empty_Fields()
	{
	//privacy policy is already tick here for this test case.
	homepage.ClickOnMyAccount();
	homepage.ClickOnregistrationButton();
	registration.FullRegistration("", "", "","", "", "");
	assertEquals(registration.verify_Error_for_Empty_Field_FirstName(),"First Name must be between 1 and 32 characters!"," Not able to verify the test case");
				
	}
	
	
	@Test
	public void Verify_Negative_Registration_With_Empty_PrivacyPolicy()
	{
	//privacy policy is not tick here for this test case.
	homepage.ClickOnMyAccount();
	homepage.ClickOnregistrationButton();
	registration.FullRegistration_without_privacyPolicy("TestFirstName", "TestLastName", "testreg@email.com","12345678910", "test", "test");
	assertEquals(registration.verify_Error_on_Empty_Privact_Policy(),"Warning: You must agree to the Privacy Policy!"," Not able to verify the test case");
				
	}
	
	@Test
	public void Verify_Negative_Registration_With_Empty_FirstName()
	{
	//privacy policy is already tick here for this test case.
	homepage.ClickOnMyAccount();
	homepage.ClickOnregistrationButton();
	registration.FullRegistration("", "TestLastName", "testreg@email.com","12345678910", "test", "test");
	assertEquals(registration.verify_Error_for_Empty_Field_FirstName(),"First Name must be between 1 and 32 characters!"," Not able to verify the test case");
		
	}
	
	
	@Test
	public void Verify_Negative_Registration_With_Empty_LastName()
	{
	//privacy policy is already tick here for this test case.
	homepage.ClickOnMyAccount();
	homepage.ClickOnregistrationButton();
	registration.FullRegistration("TestFristName", "", "testreg@email.com","12345678910", "test", "test");
	assertEquals(registration.verify_Error_for_Empty_Field_LastName(),"Last Name must be between 1 and 32 characters!"," Not able to verify the test case");
		
	}
	
	

	@Test
	public void Verify_Negative_Registration_With_Empty_Email()
	{
	//privacy policy is already tick here for this test case.
	homepage.ClickOnMyAccount();
	homepage.ClickOnregistrationButton();
	registration.FullRegistration("TestFirstName", "TestLastName", "","12345678910", "test", "test");
	assertEquals(registration.verify_Error_for_Empty_Field_Email(),"E-Mail Address does not appear to be valid!"," Not able to verify the test case for empty email");
				
	}
	
	@Test
	public void Verify_Negative_Registration_With_Empty_Phone()
	{
	//privacy policy is already tick here for this test case.
	homepage.ClickOnMyAccount();
	homepage.ClickOnregistrationButton();
	registration.FullRegistration("TestFirstName", "TestLastName", "testreg@email.com","", "test", "test");
	assertEquals(registration.verify_Error_for_Empty_Field_Telephone(),"Telephone must be between 3 and 32 characters!"," Not able to verify the test case for empty phone");
				
	}
	
	@Test
	public void Verify_Negative_Registration_With_Empty_Passeord()
	{
	//privacy policy is already tick here for this test case.
	homepage.ClickOnMyAccount();
	homepage.ClickOnregistrationButton();
	registration.FullRegistration("TestFirstName", "TestLastName", "testreg@email.com","12345678910", "", "test");
	assertEquals(registration.verify_Error_for_Empty_Field_Password(),"Password must be between 4 and 20 characters!"," Not able to verify the test case for empty password");
				
	}
	
	@Test
	public void Verify_Negative_Registration_With_Empty_ConfirmPass()
	{
	//privacy policy is already tick here for this test case.
	homepage.ClickOnMyAccount();
	homepage.ClickOnregistrationButton();
	registration.FullRegistration("TestFirstName", "TestLastName", "testreg@email.com","12345678910", "test", "");
	assertEquals(registration.verify_Error_for_Empty_Field_ConfirmPass(),"Password confirmation does not match password!"," Not able to verify the test case for empty comfirmpass");
				
	}
	
//	Skip this test because need to how to use toolkit as Webelement
//	@Test
//	public void Verify_Negative_registration_with_Wrong_Email() {
//	//privacy policy is already tick here for this test case.
//	homepage.ClickOnMyAccount();
//	homepage.ClickOnregistrationButton();
//	registration.FullRegistration("TestFirstName", "TestLastName", "testemail","12345678910", "test", "");
//	assertEquals("",""," Not able to verify the test case for wrong email");	//for toolkit	
//		
//	}
//	
	@Test
	public void Verify_Negative_registration_for_unmatch_password_and_confirmPassword() {
	//privacy policy is already tick here for this test case.
	homepage.ClickOnMyAccount();
	homepage.ClickOnregistrationButton();
	registration.FullRegistration("TestFirstName", "TestLastName", "testreg@email.com","12345678910", "test2", "test");
	assertEquals(registration.verify_Error_for_Empty_Field_ConfirmPass(),"Password confirmation does not match password!"," Not able to verify the test case for unmatch password");
			
	}
	
	@Test
	public void Verify_Positive_registration_for_valid_input() {
	//privacy policy is already tick here for this test case.
	homepage.ClickOnMyAccount();
	homepage.ClickOnregistrationButton();
	registration.FullRegistration("TestFirstName", "TestLastName", "testreg11@email.com","12345678910", "test", "test");
	
	//get page title after registration to ensure the registration
	String PageTileAfterRegistration = driver.getTitle();
	assertEquals(PageTileAfterRegistration,"Your Account Has Been Created!","Not able to verify the test case for valid input");
			
	}
	
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
	
	
	@AfterTest
	public void afterTest() {
		driver.close();
	}
	
	
	

	

}
